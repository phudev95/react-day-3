export const ScreensUser = () => {
  return (
    <div className="screens-user">
      <h2>User List</h2>
      <p className="env-content">
        REACT_APP_ENV_KEY_BY_DOMAIN: {process.env.REACT_APP_ENV_KEY_BY_DOMAIN}
      </p>
      <img src="/assets/images/cicd-variables.png" alt="variables" />
    </div>
  );
};
